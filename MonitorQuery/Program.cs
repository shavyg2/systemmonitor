﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using System.Threading;
using System.Collections.Concurrent;
using Commander;
using MongoDB.Driver.Builders;


namespace MonitorQuery
{
    public class Program
    {

        public static dynamic cmd;


        static void Main(string[] args)
        {


            cmd = new Command();
            cmd.parse(args);

            cmd.set("", "hostname", "Filter Results By Hostname", true)
                .set("", "mac", "Filter Results base on Mac Address", true)
                .set("", "timestamp", "Time Stamp to filter by", true)

                .set("", "days", "Filter Results base on days passed", true)
                .set("", "hours", "Filter Results base on hours passed", true)
                .set("", "mins", "Filter Results base on mins passed", true)
                .set("", "useEnter", "Press Enter to view results one by one", false)
                .set("h", "help", "Press Enter to View one by one", false)
                .set("", "error", "Press Enter to View one by one", false)
                .set("", "warning", "Press Enter to View one by one", false)
                .set("", "info", "Press Enter to View one by one", false)
                ;

            if (cmd.help)
            {
                Console.WriteLine("A small color coded tool to view, event logs. There are various to filter what is being seen on the page.\nAdditional Questions see Author\nAuthor: Shavauhn Gabay");
                Console.WriteLine();
                cmd.printHelp();
                return;
            }

            var dbnames = Database.client.GetServer().GetDatabaseNames();

            var evtlog = Database.client.GetServer().GetDatabase("event-logs");


            var collection = evtlog.GetCollection("quinte");

            int days = 0, hours = 0, mins = 0;
            long time;
            if (cmd.hours != null || cmd.mins != null || cmd.secs != null)
            {
                Int32.TryParse(cmd.days, out days);
                Int32.TryParse(cmd.hours, out hours);
                Int32.TryParse(cmd.mins, out mins);

                time = ConvertToTimestamp(DateTime.Now.AddDays(-1 * days).AddHours(-1 * hours).AddMinutes(-1 * mins));
            }
            else {
                time = 0;
            }

            

            var query= Query.GTE("timestamp", BsonValue.Create(time));
            var all = collection.Find(query);




            var terminal = new Dictionary<string, IList<BsonValue>>();

            var processing = true;
            var total = all.Count();
            var total_processed = 0d;
            var percentage = 0d;

            var process_t = new Thread(new ThreadStart(() =>
            {
                int i = 0;
                int dots = 4;
                do
                {
                    percentage = Math.Round(total_processed / total, 2);
                    string s = string.Empty;
                    for (int j = 0; j < i % dots; j++)
                    {
                        s += ".";
                    }
                    Console.Write("\rProcessing{0}           ", s);
                    i++;

                    Thread.Sleep(333);
                } while (processing);

                Console.WriteLine();
            }));

            process_t.Start();

            var docs = all.ToArray();
            var obj = new Object();
            Parallel.For(0, all.Count() - 1, (x) =>
            {
                total_processed += 1;
                var doc = docs[x];
                if (doc["payload"].BsonType != BsonType.Document || !doc["payload"].AsBsonDocument.Contains("MAC"))
                {
                    return;
                }
                var mac = doc["payload"].AsBsonDocument["MAC"].AsString;

                IList<BsonValue> list;

                lock (obj)
                {
                    if (terminal.ContainsKey(mac))
                    {
                        list = terminal[mac];
                    }
                    else
                    {
                        list = new List<BsonValue>();
                        terminal.Add(mac, list);
                    }
                    list.Add(doc);
                }


            });




            BsonValue[] issues;

            if (cmd.hostname != null)
            {
                issues = HostName(terminal, (string)cmd.hostname).ToArray();
            }
            else if (cmd.mac != null)
            {
                issues = HostName(terminal, (string)cmd.hostname).ToArray();
            }
            else
            {
                issues = GetBsonValue(terminal);
            }
            var result=Process(issues);

            processing = false;
            process_t.Join();

            DisplayResult(result);

            Console.WriteLine("Completed press any key to exit");
#if DEBUG
            Console.ReadLine();
#endif
        }


        public static BsonValue[] GetBsonValue(Dictionary<string, IList<BsonValue>> terminal)
        {
            var lists = new List<BsonValue>();
            foreach (var key in terminal.Keys)
            {
                foreach (var item in terminal[key])
                {
                    lists.Add(item);
                }
            }
            return lists.ToArray();
        }

        public static IEnumerable<BsonValue> Process(IList<BsonValue> terminal)
        {
            var obj = new object();
            var docs = terminal.ToArray();
            var cursor = docs.Where((doc) =>
            {
                var keep = true;
                if (cmd.timestamp != null)
                {
                    long time = 0;
                    Int64.TryParse(cmd.timestamp, out time);

                    if (doc["timestamp"].AsInt32 < time)
                    {
                        keep = false;
                    }
                }

                var days = 0;
                var hours = 0;
                var mins = 0;


                /*
                if (cmd.hours != null || cmd.mins != null || cmd.secs != null)
                {
                    Int32.TryParse(cmd.days, out days);
                    Int32.TryParse(cmd.hours, out hours);
                    Int32.TryParse(cmd.mins, out mins);

                    long time = ConvertToTimestamp(DateTime.Now.AddDays(-1 * days).AddHours(-1 * hours).AddMinutes(-1 * mins));

                    if (doc["timestamp"].AsInt32 < time)
                    {
                        keep = false;
                    }
                }
                 * */

                if (cmd.error || cmd.warning || cmd.info)
                {
                    if ((cmd.error && !isError(doc)) || (cmd.warning && !isWarning(doc)) || (cmd.info && !isInformation(doc)))
                    {
                        keep = false;
                    }
                }

                return keep;
            });

            return cursor;
        }

        public static void DisplayResult(IEnumerable<BsonValue> cursor){
            
            foreach (var doc in cursor)
            {
                if (doc["payload"]["Description"].AsString.IndexOf("Event Type:Information") > -1)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if (doc["payload"]["Description"].AsString.IndexOf("Event Type:Error") > -1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else if (doc["payload"]["Description"].AsString.IndexOf("Event Type:Warning") > -1)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine("{0}\n{1}\n{2}", doc["payload"]["MAC"].AsString, doc["payload"]["Hostname"].AsString, doc["payload"]["Description"].AsString);

                Console.ForegroundColor = ConsoleColor.White;
                if (cmd.useEnter)
                {
                    Console.ReadLine();
                }
            }
        }

        public static bool isError(BsonValue doc)
        {
            return (doc["payload"]["Description"].AsString.IndexOf("Event Type:Error") > -1);
        }

        public static bool isInformation(BsonValue doc)
        {
            return (doc["payload"]["Description"].AsString.IndexOf("Event Type:Information") > -1);
        }

        public static bool isWarning(BsonValue doc)
        {
            return (doc["payload"]["Description"].AsString.IndexOf("Event Type:Warning") > -1);
        }

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }

        #region processing
        public static int Count(Dictionary<string, IList<BsonValue>> terminal, string mac)
        {
            int count = 0;
            if (terminal.ContainsKey(mac))
            {
                count = terminal[mac].Count;
            }
            return count;
        }

        public static IList<BsonValue> MacList(Dictionary<string, IList<BsonValue>> terminal, string mac)
        {
            if (terminal.ContainsKey(mac))
            {
                return terminal[mac];
            }
            else
            {
                return new List<BsonValue>();
            }
        }

        public static IList<BsonValue> HostName(Dictionary<string, IList<BsonValue>> terminal, string hostname)
        {
            foreach (var key in terminal.Keys)
            {
                if (terminal[key].Count > 0 && terminal[key][0]["payload"].AsBsonDocument["Hostname"].AsString == hostname)
                {
                    return terminal[key];
                }
            }
            return new List<BsonValue>();
        }
        #endregion


    }
}

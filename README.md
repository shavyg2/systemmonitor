#SystemMonitor

System Monitor is a Library that allows you to monitor various parts of a computer system. The Library is made up of some building blocks to help develop this library.


The main interface of this library is located here:

[IMonitor](https://bitbucket.org/shavyg2/systemmonitor/src/108bcc6d6b6fca1ddff824a65e29180637bd72fe/SystemMonitor.cs/IMonitor.cs?at=master)

This Interface exposes certain methods that a developer must implement in order to allow the class to run.

	public MonitorEvent GetData();

This method allows a developer to gather data and place it in the MonitorEvent class.

This allows a user to attached information that they may use later on to send data to a server or write to a file. *Note this class is likely to change to make it more dynamic in the information that it can send to the server. The change won't affect how the class is implemented however.*

-----------
	bool SendData(MonitorEvent e);

This method is used to send data to a server or perhaps to save it to a file. Which ever the case this is the method to save data.

------------

	 void DataFailed(MonitorEvent evt);
In the event that the data failed to be sent you may want to implement this method and have it do something else with the data. You may leave this method blank if you have no recovery strategy.

---------

You can check the Source Code for IMonitor Interface

If you choose to understand how this interface is used you can check out the MonitorManager class.
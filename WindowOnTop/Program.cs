﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemMonitor;

namespace WindowOnTop
{

    class OnTop : IMonitor {


        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll")]
        public static extern IntPtr SetActiveWindow(IntPtr hWnd);

        private IntPtr handle;

        string process_name;
        public OnTop(string name) {
            process_name = name;
        }



        public MonitorEvent GetData()
        {
            var procs = Process.GetProcessesByName(process_name);
            if (procs.Length > 0) {
                foreach (var pro in procs) {
                    while(!SetForegroundWindow(pro.MainWindowHandle))
                    SetActiveWindow(pro.MainWindowHandle); 
                    
                    
                    

#if DEBUG
                    Console.WriteLine(pro.ProcessName);
                    Console.WriteLine(pro.MainWindowTitle);

#endif

                }
            }

            return null;
        }

        public bool SendData(MonitorEvent e)
        {
            return true;
        }

        public void DataFailed(MonitorEvent evt)
        {
        }

        public void RecoverData(MonitorEvent evt)
        {
            
        }

        public void Store(MonitorEvent evt)
        {
            
        }

        public MonitorEvent[] Load()
        {
            return null;
        }

        public long RecoveryTimeout()
        {
            return 2500L;
        }

        public bool isRunning()
        {
            return true;
        }

        public bool ShouldStore(MonitorEvent data)
        {
            return false;
        }

        public void HandleNotRunning()
        {

        }

        public bool Dispose()
        {
            return true;
        }

        public long GetTimeout()
        {
            return RecoveryTimeout();
        }

        public bool isRunOnce()
        {
            return false;
        }

        public dynamic GetSettings()
        {
            return null;
        }

        public void ApplySettings(dynamic settings)
        {
            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
           
            var procs = Process.GetProcesses();

            foreach (var pro in procs) {
                continue;
                Console.WriteLine(pro.ProcessName);
                Console.WriteLine(pro.MainWindowTitle);

                Console.WriteLine();

            }

            var mm = new MonitorManager();
            //mm.AddMonitor(new ProgramMonitor("chrome"));
            mm.AddMonitor(new OnTop("wfica32"));
            mm.StartMonitoring();
        }
    }
}

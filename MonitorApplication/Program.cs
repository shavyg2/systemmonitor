﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemMonitor;
using SystemMonitor.Monitors;

namespace SystemMonitor
{
    class Program
    {
        static void Main(string[] args){
        
            System.Net.ServicePointManager.Expect100Continue = false;
            var mm = new MonitorManager();
            //mm.AddMonitor(new ProgramMonitor("chrome"));
            mm.AddMonitor(new EventLogMonitor("Application"));
            mm.StartMonitoring();
        }
    }
}

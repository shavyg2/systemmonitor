﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using ServiceStack;
using System.Net;
using System.Net.NetworkInformation;

namespace SystemMonitor
{
    public class MonitorEvent
    {
        public MonitorEvent() {
            Date = DateTime.Now.ToUnixTime();
            Hostname = Dns.GetHostName();
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    MAC=nic.GetPhysicalAddress().ToString();
                }
            }
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public long Date { get; set; }
        public string Hostname { get; set; }
        public string MAC { get; set; }

        private int attempts = 0;
        public int Attempts { get { return attempts; } set { attempts = value; } }

        public override string ToString()
        {
            return ToJSON();
        }

        public virtual string ToJSON() {
            var json=DynamicJson.Serialize(this);
            return json.ToString();
        }
    }
}

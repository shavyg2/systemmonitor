﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using ServiceStack;
using System.Net;
using System.IO;

namespace SystemMonitor.Monitors
{
    public class EventLogMonitor : IMonitor
    {
        public string EventLogName { get; set; }

        public EventLogMonitor(string EventLogName) {
            this.EventLogName = EventLogName;
        }

        public MonitorEvent GetData()
        {
            Console.WriteLine("Monitoring EventsLog");
            EventLog log = new EventLog() {
                Log=EventLogName
            };
            var entries = log.Entries;
            var timespan = TimeSpan.FromMilliseconds(GetTimeout());
            var valid = DateTime.Now.Subtract(timespan);

            int start = GetStartIndex(entries, 0, 0, entries.Count - 1, valid,0,30);

            for (int i= start;i<entries.Count;i++) {
                var entry = entries[i];
                if (Track(entry))
                {
                    var me = new MonitorEvent()
                    {
                        Name = "EventLog",
                        Description = string.Format(@"Event Type:{0}
Event Message:{1}
TimeGenerated:{2}
EventSource:{3}
", entry.EntryType, entry.Message,entry.TimeGenerated,entry.Source)
                    };

                    SendData(me);
                }
            }
            return null;
        }

        /// <summary>
        /// Override this method to filter events event more. Make sure to call the base class method if your filter chooses to allow
        /// The was allowed.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        protected virtual bool Track(EventLogEntry entry) {
            var valid = DateTime.Now.Subtract(TimeSpan.FromMilliseconds(GetTimeout()));
            return entry.TimeGenerated > valid;
        }

        private int GetStartIndex(EventLogEntryCollection entries, int start,int front,int end,DateTime find,int depth,int maxDepth){
            if (depth == maxDepth) {
                return start;
            }
            
            if (front == end) {
                return start;
            }
            
            if (entries[start].TimeGenerated < find)
            {
                start = start + ((end - front) / 2);
                front = start;
                return GetStartIndex(entries, start, front, end, find,++depth,maxDepth);
            }
            else{
                start = start - ((end - front) / 2);
                end = start;
                return GetStartIndex(entries, start, front, end, find,++depth, maxDepth);
            }
        }

        public static void EventFired(object source, EntryWrittenEventArgs e)
        {
            
        }


        public bool SendData(MonitorEvent e)
        {
            dynamic obj= new ExpandoObject();
            dynamic identifier = new ExpandoObject();
            identifier.type = "MAC";
            identifier.value = e.MAC;


            obj.identifier = identifier;
            obj.timestamp = e.Date;
            obj.payload = e;

            var text=DynamicJson.Serialize(obj);

            HttpWebRequest http = (HttpWebRequest)WebRequest.Create("http://162.209.79.120:29017/event-logger/quinte/");
            http.Method = "POST";
            http.ContentType = "application/json";


            Stream postStream = http.GetRequestStream();
            byte[] buffer= Encoding.UTF8.GetBytes(text);
            postStream.Write(buffer, 0, buffer.Length);
            postStream.Flush();
            postStream.Close();

            var res = http.GetResponse();
            var resStream = res.GetResponseStream();
            var resText = new StreamReader(resStream).ReadToEnd();
            resStream.Close();
            res.Close();
#if test
            Console.WriteLine(resText);
#endif
            return true;
        }

        public void DataFailed(MonitorEvent evt)
        {
           //don't caree
        }

        public void RecoverData(MonitorEvent evt)
        {
            //don't care
        }

        public void Store(MonitorEvent evt)
        {
            //don't care
        }

        public MonitorEvent[] Load()
        {
            //don't care return null
            return null;
        }

        public long RecoveryTimeout()
        {
            return (long)new TimeSpan(1,0,0).TotalMilliseconds;
        }

        public bool isRunning()
        {
            return true;
        }

        public bool ShouldStore(MonitorEvent data)
        {
            return false;
        }

        public void HandleNotRunning()
        {
            //don't care
        }

        public bool Dispose()
        {
            //nothing to dispose
            return true;
        }

        public long GetTimeout()
        {
            return RecoveryTimeout();
        }

        public bool isRunOnce()
        {
            return false;
        }

        public dynamic GetSettings()
        {
            //don't care
            return null;
        }

        public void ApplySettings(dynamic settings)
        {
            //don't care
        }
    }
}

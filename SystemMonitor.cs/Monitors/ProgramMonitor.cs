﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemMonitor.Monitors
{
    public class ProgramMonitor : IMonitor
    {
        protected string ProgramName { get; set; }
        private Process p = null;
        protected Process Process { get { return p; } }

        public ProgramMonitor(string ProgramName)
        {
            this.ProgramName = ProgramName;

            #region Process Handling
            if (p == null)
            {
                var procs = Process.GetProcessesByName(this.ProgramName);
                if (procs.Length > 0)
                {
                    p = procs[0];
                }
            }
            #endregion
        }


        public virtual MonitorEvent GetData()
        {
            if (Process != null)
            {
                var Evt = new MonitorEvent()
                {
                    Name = "Program Monitoring",
                    Attempts = 0,
                    Date = new DateTime().ToBinary(),
                    Description = String.Format("Process:{0}\nMemory Usage:{1}\nPeak Usage:{2}", Process.ProcessName, Process.VirtualMemorySize64, Process.PeakVirtualMemorySize64)
                };

                return Evt;
            }
            else
            {
                var Evt = new MonitorEvent()
                {
                    Name = "Program Monitoring",
                    Attempts = 0,
                    Date = new DateTime().ToBinary(),
                    Description = String.Format("Process:{0} is not running", ProgramName)
                };

                return Evt;
            }

        }

        public virtual bool SendData(MonitorEvent evt)
        {
            Console.WriteLine(evt);
            return true;
        }

        public virtual void DataFailed(MonitorEvent evt)
        {
            
        }

        public virtual void RecoverData(MonitorEvent evt)
        {
            
        }

        public virtual long RecoveryTimeout()
        {
            return GetTimeout();   
        }

        public virtual long GetTimeout()
        {
            return 5000;
        }

        public virtual dynamic GetSettings()
        {
            //get some stuff from the server
            return null;
        }

        public virtual void ApplySettings(dynamic settings)
        {
            //change how this class works with some settings
        }


        public bool isRunning()
        {
            return !Process.HasExited;
        }

        public bool Dispose()
        {
            //free what ever
            return true;
        }


        public void Store(MonitorEvent evt)
        {
            Console.WriteLine("saving... " + evt.Name);
        }

        public MonitorEvent[] Load()
        {
            //grab the stored data and do something with it.
            return null;
        }

        public bool ShouldStore(MonitorEvent data)
        {
            return false;
        }

        public bool isRunOnce()
        {
            return false;
        }


        public void HandleNotRunning()
        {
            //if it stops running do something with this information
        }
    }
}

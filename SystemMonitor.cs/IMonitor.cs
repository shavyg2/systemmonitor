﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemMonitor;

namespace SystemMonitor
{
    public interface IMonitor
    {

        /// <summary>
        /// Gets the data from the system 
        /// </summary>
        /// <returns></returns>
        MonitorEvent GetData();


        /// <summary>
        /// Send data to the server that should be logged.
        /// </summary>
        /// <returns></returns>
        bool SendData(MonitorEvent e);


        /// <summary>
        /// What should be done in the event that data could not be successfully sent to the server.
        /// </summary>
        void DataFailed(MonitorEvent evt);

        /// <summary>
        /// In the event that the system fails to send this data to the network it will need to recover it and send it at a later point in time
        /// </summary>
        void RecoverData(MonitorEvent evt);


        void Store(MonitorEvent evt);


        MonitorEvent[] Load();


        /// <summary>
        /// The time period to wait before attempting to recover data.
        /// </summary>
        long RecoveryTimeout();



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool isRunning();


        bool ShouldStore(MonitorEvent data);



        void HandleNotRunning();


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Dispose();



        /// <summary>
        /// Gets the amount of time to wait before attempting to log this information again.
        /// </summary>
        /// <returns>The amount of time to wait to log this event again</returns>
        long GetTimeout();



        bool isRunOnce();

        //So we can change how this program will work in the future
        dynamic GetSettings();
        void ApplySettings(dynamic settings);
    }
}

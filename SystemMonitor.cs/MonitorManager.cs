﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SystemMonitor
{
    /// <summary>
    /// This class will monitor all of the different Monitor Classes we have running.
    /// </summary>
    public class MonitorManager
    {
        protected IList<IMonitor> Monitors { get; set; }

        public MonitorManager()
        {
            Monitors = new List<IMonitor>();
        }

        #region Monitor Manager

        public void AddMonitor(IMonitor monitor)
        {
            Monitors.Add(monitor);
        }

        public void RemoveMonitor(IMonitor monitor)
        {
            Monitors.Remove(monitor);
        }

        #endregion

        public void StartMonitoring()
        {
            var t = new Thread(new ThreadStart(BeginMonitoring));
            t.Start();
        }

        private void BeginMonitoring()
        {
            foreach (var monitor in Monitors)
            {
                new Thread(new ParameterizedThreadStart(Each)).Start(monitor);
            }
        }

        private void Each(object obj) {
            IMonitor monitor = (IMonitor)obj;

                    while (true)
                    {
                        var data = monitor.GetData();
                        if (monitor.isRunning())
                        {
                            try
                            {
                                monitor.SendData(data);
                            }
                            catch (Exception e)
                            {
                                monitor.DataFailed(data);
                                if (monitor.ShouldStore(data))
                                {
                                    monitor.Store(data);
                                }
                            }


                            if (monitor.Load() != null)
                            {
                                /*
                                 * Make sure to use locks in the monitor class to prevent files being open at the same time.
                                 */
                                new Thread(new ThreadStart(delegate()
                                {
                                    var evts = monitor.Load();
                                    if (evts != null)
                                    {
                                        foreach (var evt in evts)
                                        {
                                            try
                                            {
                                                monitor.SendData(evt);
                                            }
                                            catch (Exception)
                                            {
                                                //For some reasom this is not working
                                            }
                                        }
                                    }
                                })).Start();
                            }
                            if (monitor.isRunOnce())
                            {
                                break;
                            }
                            else
                            {
                                Thread.Sleep((int)monitor.GetTimeout());
                            }
                        }
                        else
                        {
                            monitor.HandleNotRunning();
                        }
                    }
                
        
        }
    }
}
